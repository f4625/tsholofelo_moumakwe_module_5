import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:module_5/sessionList.dart';

class AddSession extends StatefulWidget {
  const AddSession({Key? key}) : super(key: key);

  @override
  State<AddSession> createState() => _AddSessionState();
}

class _AddSessionState extends State<AddSession> {
  @override
  Widget build(BuildContext context) {
    TextEditingController guardController = TextEditingController();
    TextEditingController suburbController = TextEditingController();
    TextEditingController houseController = TextEditingController();

    Future _addSessions() {
      final guard = guardController.text;
      final suburb = suburbController.text;
      final house = houseController.text;

      final ref = FirebaseFirestore.instance.collection("schedules").doc();

      return ref
          .set({
            "Guard_Name": guard,
            "Street_Suburb": suburb,
            "House_Number": house,
            "doc_id": ref.id
          })
          .then((value) => {
                guardController.text = "",
                suburbController.text = "",
                houseController.text = ""
              })
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                  controller: guardController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular((20))),
                      hintText: "Enter Name and Surname")),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                  controller: suburbController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular((20))),
                      hintText: "Enter Street Name and Suburb")),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                  controller: houseController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular((20))),
                      hintText: "Enter House Number")),
            ),
            ElevatedButton(
                onPressed: () {
                  _addSessions();
                },
                child: Text("Add Session"))
          ],
        ),
        SessionList()
      ],
    );
  }
}
