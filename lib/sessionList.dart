import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import "package:firebase_core/firebase_core.dart";

class SessionList extends StatefulWidget {
  const SessionList({Key? key}) : super(key: key);

  @override
  State<SessionList> createState() => _SessionListState();
}

class _SessionListState extends State<SessionList> {
  final Stream<QuerySnapshot> _mySessions =
      FirebaseFirestore.instance.collection("schedules").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _guardFieldCtrlr = TextEditingController();
    TextEditingController _suburbFieldCtrlr = TextEditingController();
    TextEditingController _houseFieldCtrlr = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("schedules")
          .doc(docId)
          .delete()
          .then((value) => print("deleted"));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("schedules");
      _guardFieldCtrlr.text = data["Guard_Name"];
      _suburbFieldCtrlr.text = data["Street_Suburb"];
      _houseFieldCtrlr.text = data["House_Number"];

      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("Update"),
                content: Column(mainAxisSize: MainAxisSize.min, children: [
                  TextField(
                    controller: _guardFieldCtrlr,
                  ),
                  TextField(
                    controller: _suburbFieldCtrlr,
                  ),
                  TextField(
                    controller: _houseFieldCtrlr,
                  ),
                  TextButton(
                      onPressed: () {
                        collection.doc(data["doc_id"]).update({
                          "Guard_Name": _guardFieldCtrlr.text,
                          "Street_Suburb": _suburbFieldCtrlr.text,
                          "House_Number": _houseFieldCtrlr.text
                        });
                        Navigator.pop(context);
                      },
                      child: Text("Update"))
                ]),
              ));
    }

    return StreamBuilder(
      stream: _mySessions,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("Not Good");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                      height: (MediaQuery.of(context).size.height),
                      width: (MediaQuery.of(context).size.width),
                      child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot documentSnapshot) {
                          Map<String, dynamic> data =
                              documentSnapshot.data() as Map<String, dynamic>;
                          return Column(children: [
                            Card(
                              child: Column(
                                children: [
                                  ListTile(
                                    title: Text(data['Guard_Name']),
                                    subtitle: Text(data['Street_Suburb']),
                                    trailing: Text(data["House_Number"]),
                                  ),
                                  ButtonTheme(
                                      child: ButtonBar(
                                    children: [
                                      OutlineButton.icon(
                                        onPressed: () {
                                          _update(data);
                                        },
                                        icon: Icon(Icons.edit),
                                        label: Text("Edit"),
                                      ),
                                      OutlineButton.icon(
                                        onPressed: () {
                                          _delete(data["doc_id"]);
                                        },
                                        icon: Icon(Icons.remove),
                                        label: Text("Delete"),
                                      )
                                    ],
                                  ))
                                ],
                              ),
                            )
                          ]);
                        }).toList(),
                      )))
            ],
          );
        } else {
          return (Text("No data"));
        }
      },
    );
  }
}
