import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:module_5/addSession.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyBhAR1Nf8i0eW3kAjb0439jP5Arh-0oFTg",
          authDomain: "module5app.firebaseapp.com",
          projectId: "module5app",
          storageBucket: "module5app.appspot.com",
          messagingSenderId: "206821844112",
          appId: "1:206821844112:web:b7ae734864f15331a36415"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: const MyHomePage(title: 'Kasi Watch Sessions'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[AddSession()],
        ),
      ),
    );
  }
}
